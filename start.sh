#!/usr/bin/env bash

# Start Gunicorn process:
echo "Starting Gunicorn"
exec pipenv run gunicorn api.wsgi:application \
    --bind 0.0.0.0:8000 \
    --worker 3