from django.urls import reverse
from rest_framework.test import APIClient, APITestCase
from rest_framework.views import status

from .models import WorldSkills
from .serializers import WorldSkillsSerializer

# Create your tests here.


class BaseViewTest(APITestCase):
    """Use this class to create a test view to use for future tests.

    This class contains functions and variables for setting up a test view for
    the tests for this application.
    """

    client = APIClient()

    @staticmethod
    def create_world_skill(name="", desc=""):
        """Use this function to create a test instance of the WorldSkills.

        This function is used for creating an instance of the 'WorldSkills'
        object for the tests.

        :param name: the name of the WorldSkills instance, defaults to "".
        :param name: str, optional
        :param desc: the description of the WorldSkills instance, defaults to
                     "".
        :param desc: str, optional
        :return: a new test instance of the WorldSkills object.
        :rtype: WorldSkills
        """
        if name != "" and desc != "":
            return WorldSkills.objects.create(name=name, description=desc)

    def setUp(self):
        """Use this function to create all of the data to be used for tests.

        This function is used for setting up all of the data used for the tests
        for this application.
        """
        self.create_world_skill('attack', 'Boosts max attack.')
        self.create_world_skill('defence', 'Boost max defence.')


class GetAllWorldSkillsTest(BaseViewTest):
    """Use this class for testing the app class for getting all objects.

    This class contains the functions used for testing the 'WorldSkills' app
    for getting all of the objects.
    """

    def test_get_all_skills(self):
        """Use this function to test getting all of the Models.

        This function is used for getting all of the test app objects and
        testing the response and the data returned.
        """
        response = self.client.get(
            reverse("skills-all", kwargs={"version": "v1"})
        )
        # fetch the data from db
        expected = WorldSkills.objects.all()
        serialized = WorldSkillsSerializer(expected, many=True)
        self.assertEqual(response.data, serialized.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
