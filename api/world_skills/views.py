from rest_framework import generics

from .models import WorldSkills
from .serializers import WorldSkillsSerializer


# Create your views here.
class ListWorldSkillsView(generics.ListAPIView):
    """Use this class to define a get class method.

    This class contains the serialiser and the queryset for getting
    'WorldSkills' instances.
    """

    queryset = WorldSkills.objects.all()
    serializer_class = WorldSkillsSerializer
