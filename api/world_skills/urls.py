from django.urls import path

from .views import ListWorldSkillsView

urlpatterns = [
    path('world/skills/', ListWorldSkillsView.as_view(), name="skills-all")
]
