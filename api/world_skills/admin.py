from django.contrib import admin

from .models import WorldSkills

# Register your models here.
admin.site.register(WorldSkills)
