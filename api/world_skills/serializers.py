from rest_framework import serializers

from .models import WorldSkills


class WorldSkillsSerializer(serializers.ModelSerializer):
    """Use this class for serialising and deserialising the WorldSkills class.

    This function is used for serialising and deserialising the 'WorldSkills'
    class instance to and from the database.
    """

    class Meta:
        """Use this class to define the serialiser Meta data.

        This function is used for defining the serialiser Meta data for the
        'WorldSkills' class instances.
        """

        model = WorldSkills
        fields = ("name", "description")
