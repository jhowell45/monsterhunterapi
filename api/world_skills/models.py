from django.db import models

# Create your models here.


class WorldSkills(models.Model):
    """Use this class to define the database Model for the skills Model.

    This function is used for defining the database Model for the Monster
    Hunter World skills Model.
    """

    name = models.CharField(max_length=255, null=False)
    description = models.CharField(max_length=255, null=False)

    def __repr__(self):
        """Use this function to represent an instance of the class.

        This function is used for creating a representation for the
        'WorldSkills' class instance.

        :return: representation for the 'WorldSkills' class.
        :rtype: str
        """
        return (
            '<WorldSkills Name: {}, Description: {}>'.format(
                self.name, self.description
            )
        )
