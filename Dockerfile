# Dockerfile

# Base image:
FROM python:3.7-alpine

# Project Files and Settings
ARG PROJECT=monsterhunterapi
ARG PROJECT_DIR=/var/www/${PROJECT}
ARG DJANGO_DIR=/var/www/${PROJECT}/api

# Create work directory:
RUN mkdir -p $PROJECT_DIR
WORKDIR $PROJECT_DIR

# Install application packages:
COPY Pipfile Pipfile.lock ./
RUN pip install -U pipenv
RUN pipenv install --system --deploy

# Server:
WORKDIR $DJANGO_DIR
EXPOSE 8000
STOPSIGNAL SIGINT

ENTRYPOINT ["python", "manage.py"]
CMD ["runserver", "0.0.0.0:8000"]
# CMD ["gunicorn", "api.wsgi:application", "--bind", "0.0.0.0:8000"]